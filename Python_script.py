
# coding: utf-8

# # Importing libraries

# In[1]:


import pandas as pd 
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
from itertools import chain


# In[2]:


os.chdir('D:\onboarding work\exercise\python\survey')
arr = os.listdir()
arr
df1 = pd.read_csv("survey.csv" , na_values=" NaN" )
df2 = pd.read_csv("country_map.csv", sep=',',names = ["id","name"], na_values=" NaN")
#df1.head(4)
#df2.head(4)
#df1.head(3)


# # Merge country_map to the survey using respondent id as an index

# In[3]:


df = pd.merge(df1, df2, how = 'left' ,left_on = 'Respondent', right_on = 'id')
#df.head(4)# 


# In[4]:


opensource = df[df['OpenSource'] == 'Yes']
opensource = opensource.groupby('name', as_index=False)['OpenSource'].count()
opensource = opensource.sort_values('OpenSource' ,ascending=False)
opensource_graph = opensource.head(10)
opensource.head(10)


# # People from which country contributes the most to the opensource community

# In[5]:


opensource.loc[opensource['OpenSource'].idxmax()][0]


# In[6]:


open_graph = df[df['OpenSource'] == 'Yes']
open_graph = open_graph.groupby(['name','Gender'], as_index=False)['OpenSource'].count()
open_graphm = open_graph[open_graph['Gender'] == 'Male']
open_graphf = open_graph[open_graph['Gender'] == 'Female']
open_graph = pd.merge(open_graphm, open_graphf, how = 'left' ,on = 'name')
open_graph = open_graph.drop(open_graph.columns[[1,3]], axis=1)
open_graph.columns = ['country name', 'male number' , 'female number']
open_graph = open_graph.fillna(0)
open_graph['total'] = open_graph['male number'] + open_graph['female number']
open_graph = open_graph.sort_values('total' ,ascending=False)
open_graph = open_graph.head(10)
open_graph


# # Plot a stacked bar graph of top 10 countries by gender

# In[7]:


open_graph[['male number','female number']].plot(kind='bar', stacked=True)
plt.show()


# # Which age group uses Adblocker the most

# In[8]:


AdBlocker = df[df['AdBlocker'] == 'Yes']
AdBlocker = AdBlocker.groupby('Age',as_index=False)['AdBlocker'].count()
AdBlocker.loc[AdBlocker['AdBlocker'].idxmax()][0]


# # Top reasons for participating in a hackathon in India

# In[9]:


hgf = df[df['name'] == 'India']
hgf = hgf[['name','HackathonReasons']]
hgf = hgf[hgf['HackathonReasons'].notnull()]
pet = []
[pet.append(x) for x in chain(*hgf.HackathonReasons.str.split(";").fillna("0"))]
hgf = pd.DataFrame( {'HackathonReasons':[x for x in list(set(pet))],'count' : [pet.count(x) for x in list(set(pet))]})
hgf = hgf.sort_values('count' ,ascending=False)
hgf.head(3)


# In[10]:


hck = hgf.head(5)
title = plt.title('Top reasons for participating in a hackathon in India')
labels = hck['HackathonReasons']
explode = (0.2, 0.1,0,0,0)
plt.pie(hck['count'], explode= explode,
        autopct='%0.1f%%', shadow=True, startangle=140)
plt.legend(labels, loc="lower left" , fontsize=10, bbox_to_anchor=(1,0.2)) 
plt.subplots_adjust(left= 0, bottom= 0.1, right=1)
plt.axis('equal')
plt.show()


# 
# # Top reasons for participating in a hackathon in USA

# In[11]:


hgf = df[df['name'] == 'United States']
hgf = hgf[['name','HackathonReasons']]
hgf = hgf[hgf['HackathonReasons'].notnull()]
pet = []
[pet.append(x) for x in chain(*hgf.HackathonReasons.str.split(";").fillna("0"))]
hgf = pd.DataFrame( {'HackathonReasons':[x for x in list(set(pet))],'count' : [pet.count(x) for x in list(set(pet))]})
hgi = hgf.sort_values('count' ,ascending=False)
hgi.head(3)


# In[12]:


hci = hgi.head(5)
labels = hci['HackathonReasons']
explode = (0.2, 0.1,0,0,0)
plt.pie(hci['count'], explode= explode,
        autopct='%0.1f%%', shadow=True, startangle=140)
plt.legend(labels, loc="lower left" , fontsize=10, bbox_to_anchor=(1,0.2)) 
plt.subplots_adjust(left= 0, bottom= 0.1, right=1)
plt.axis('equal')
plt.show()


# # Top reasons for participating in a hackathon Worldwide

# In[13]:


HackathonReasons = df[['name','HackathonReasons']]
HackathonReasons = HackathonReasons[HackathonReasons['HackathonReasons'].notnull()]
pet = []
[pet.append(x) for x in chain(*HackathonReasons.HackathonReasons.str.split(";").fillna("0"))]
HackathonReasons = pd.DataFrame( {'HackathonReasons':[x for x in list(set(pet))],'count' : [pet.count(x) for x in list(set(pet))]})
HackathonReasons = HackathonReasons.sort_values('count' ,ascending=False)
HackathonReasons.head(3)


# In[14]:


hck = HackathonReasons.head(5)
labels = hck['HackathonReasons']
explode = (0.2, 0.1,0,0,0)
plt.pie(hck['count'], explode= explode,
        autopct='%0.1f%%', shadow=True, startangle=140)
plt.legend(labels, loc="lower left" , fontsize=10, bbox_to_anchor=(1,0.2)) 
plt.subplots_adjust(left= 0, bottom= 0.1, right=1)
plt.axis('equal')
plt.show()


# # Most popular OS worldwide

# In[15]:


OS = df.groupby('OperatingSystem',as_index=False)['name'].count()
OS =  OS.sort_values('name' ,ascending=False)
'Worldwide >> ' +  OS.loc[OS['name'].idxmax()][0]


# # Most popular OS in India

# In[16]:


OSI = df[df['name'] == 'India'] 
OSI = OSI.groupby('OperatingSystem',as_index=False)['name'].count()
'In india OS used most is ' + OSI.loc[OSI['name'].idxmax()][0]


# # Most popular OS in the US

# In[17]:


OSu = df[df['name'] == 'United States'] 
OSu = OSu.groupby('OperatingSystem',as_index=False)['name'].count()
'In US OS used most is ' + OSu.loc[OSu['name'].idxmax()][0]


# # Most popular IDE worldwide

# In[18]:


OS = df.groupby('IDE',as_index=False)['name'].count()
OS =  OS.sort_values('name' ,ascending=False)
'Worldwide >>' +  OS.loc[OS['name'].idxmax()][0]


# # 	Most popular IDE in India

# In[19]:


OSI = df[df['name'] == 'India'] 
OSI = OSI.groupby('IDE',as_index=False)['name'].count()
'In india IDE used most is ' + OSI.loc[OSI['name'].idxmax()][0]


# # Most popular OS in the US

# In[20]:


OSu = df[df['name'] == 'United States'] 
OSu = OSu.groupby('IDE',as_index=False)['name'].count()
'In US IDE used most is ' + OSu.loc[OSu['name'].idxmax()][0]


# # country with Highest Job satisfaction 

# In[21]:


filtered_df = df[df['JobSatisfaction'].notnull()]
JobSatisfaction = filtered_df[filtered_df['JobSatisfaction'] == 'Extremely satisfied']
JobSatisfaction = JobSatisfaction.groupby('name',as_index=False)['JobSatisfaction'].count()
JobSatisfaction = JobSatisfaction.sort_values('JobSatisfaction' ,ascending=False)
JobSatisfaction = JobSatisfaction.head(3)
JobSatisfaction.head(3)


# # Age group with Highest Job satisfaction 

# In[22]:


JobSatisfactionbyage = filtered_df.groupby('Age',as_index=False)['JobSatisfaction'].count()
JobSatisfactionbyage = JobSatisfactionbyage.sort_values('JobSatisfaction' ,ascending=False)
JobSatisfactionbyage = JobSatisfactionbyage.head(3)
JobSatisfactionbyage.head(3)


# # Popular opinion of AI India

# In[23]:


AI = df[['name','AIFuture', 'AIDangerous', 'AIInteresting','AIResponsible']]
AII = AI[AI['name'] == 'India'] 
AII.mode()


# # Popular opinion of AI USA

# In[24]:


AIU = AI[AI['name'] == 'United States'] 
AIU.mode()


# # Popular opinion of AI worldwide

# In[25]:


aiw = df[['AIFuture', 'AIDangerous', 'AIInteresting','AIResponsible']].mode()
'People thought regarding AI worldwide ' + aiw['AIFuture']
aiw = df[['AIFuture', 'AIDangerous', 'AIInteresting','AIResponsible']].mode()
aiw


# # Git vs Other VCS

# In[26]:


vcs = df[['name','VersionControl']]
vcs = vcs[vcs['VersionControl'].notnull()]
pet = []
[pet.append(x) for x in chain(*vcs.VersionControl.str.split(";").fillna("0"))]
dft = pd.DataFrame( {'vcs':[x for x in list(set(pet))],'count' : [pet.count(x) for x in list(set(pet))]})
dft = dft.sort_values('count' ,ascending=False)
dft


# # Pie chart for Git Vs other VCS

# In[27]:


labels = dft['vcs']
explode = (0, 0.1, 0, 0,0,0,0)
plt.pie(dft['count'] ,explode=explode , labels = dft['vcs'],
        autopct='%8.1f%%', shadow=True, startangle=140)
plt.legend(labels, loc="lower left" , fontsize=11, bbox_to_anchor=(1,0.2)) 
plt.subplots_adjust(left= 0, bottom= 0.1, right=1)
plt.axis('equal')
plt.show()

